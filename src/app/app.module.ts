import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Injector } from '@angular/core';
import { createCustomElement} from '@angular/elements';

import { AppRoutingModule } from './app-routing.module';
import { MessageElementComponent } from './app.custom.element';

@NgModule({
  declarations: [
    MessageElementComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  entryComponents: [MessageElementComponent]
})
export class AppModule {
  constructor(private injector: Injector) {
    const el = createCustomElement(MessageElementComponent, { injector });
    customElements.define('app-custom-element', el);
  }
  ngDoBootstrap() {
  }
}
