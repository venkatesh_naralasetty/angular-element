# AngularElement

# To see how Angular elements work on different frameworks

step 1: 

Clone the repo, After DONE! 

right click on package.json and do npm install

step 2:
Open two terminal windows

Terminal -1 : 

Run npm run start, Navigate to http://localhost:4200/. The app will automatically reload if you change any of the source files.

Here you will see the Angular element hosted on Angular7 Application from dist folder.  

step 3: 
go to Terminal-2 and run the following command

Before going further, lets makes sure you have npm lite server installed, if not 

npm install lite-server --save

After installing lite-server

step 4:
in Terminal -2

npm run build:element, Navigate to http://localhost:8000. 

Here you will see Angular element hosted on Web server with preview folder as base directory. 

Note: you might see a different image, because images changes on every load. 

# Conclusion

The idea is building angular elements independent of framework. 

a simple example how to load the same element on angularjs application. 

include the following in angularjs "index.html"

in the head tag add 

<base href="/">
<script src="http://localhost:8000/angularApp.js"></script>

in the body tag add 

<app-custom-element></app-custom-element>


re run the step4 on angular7 application.
re run the angularjs app you can then see angular element rendered on angularjs page. 







