
var _fs = require('fs');

function concat(opts) {
  var fileList = opts.src;
  var distPath = opts.dest;
  var out = fileList.map(function(filePath){
    return _fs.readFileSync(filePath).toString();
  });
  _fs.writeFileSync(distPath, out.join('\n'));
  console.log(' '+ distPath +' built.');
}

concat({
  src : [
    'dist/Angular-Element/runtime-es5.js',
    'dist/Angular-Element/polyfills-es5.js',
    'dist/Angular-Element/scripts.js',
    'dist/Angular-Element/main-es5.js',
    'dist/Angular-Element/vendor-es5.js',
    'dist/Angular-Element/styles-es5.js'
  ],
  dest : 'preview/angularApp.js'
});
